<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <artifactId>alfresco-dante-share</artifactId>
    <name>alfresco-dante-share Share Jar Module - SDK 3</name>
    <description>Share JAR Module (to be included in the share.war) - SDK 3</description>
    <packaging>jar</packaging>
    
    <parent>
        <groupId>it.drwolf.alfresco</groupId>
        <artifactId>alfresco-dante-sdk3</artifactId>
        <version>1.4.1</version>
    </parent>

    <properties>



        <!-- Since alfresco.war (i.e. the Platform/Repository) is already running on port 8080, we run Share.WAR on port 8081.
            If Alfresco Platform is not running, then generate a platform-jar-module and start it up. -->
        <maven.tomcat.port>8081</maven.tomcat.port>

    </properties>

    <dependencies>

    </dependencies>

    <build>
        <plugins>
            <!--
                The Alfresco Maven Plugin contains all the logic to run the extension
                in an embedded Tomcat with the H2 database.
                -->
            <plugin>
                <groupId>org.alfresco.maven.plugin</groupId>
                <artifactId>alfresco-maven-plugin</artifactId>
                <version>${alfresco.sdk.version}</version>
                <configuration>

                    <!-- We assume that the platform/repository is already running on localhost:8080 -->
                    <enableH2>false</enableH2>
                    <enablePlatform>false</enablePlatform>
                    <enableSolr>false</enableSolr>

                    <!-- Enable the Share webapp, which is what we customize with Share JAR modules -->
                    <enableShare>true</enableShare>

                    <!--
                       JARs and AMPs that should be overlayed/applied to the Share WAR
                       (i.e. share.war)
                   -->
                    <shareModules>
                        <!-- Bring in this JAR project, need to be included here, otherwise share-config-custom.xml
                             will not be picked up. It will not be read from target/classes/META-INF -->
                        <moduleDependency>
                            <groupId>${project.groupId}</groupId>
                            <artifactId>${project.artifactId}</artifactId>
                            <version>${project.version}</version>
                        </moduleDependency>
                        
                        <moduleDependency>
                            <groupId>de.fme.jsconsole</groupId>
                            <artifactId>javascript-console-share</artifactId>
                            <version>${jsconsole.version}</version>
                            <type>amp</type>
                        </moduleDependency>
                    </shareModules>
                </configuration>
            </plugin>

            <!-- Build an AMP if 3rd party libs are needed by the extension -->
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.6</version>
                <executions>
                    <execution>
                        <id>build-amp-file</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptor>src/main/assembly/amp.xml</descriptor>
                        </configuration>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>org.alfresco.maven.plugin</groupId>
                        <artifactId>alfresco-maven-plugin</artifactId>
                        <version>${alfresco.sdk.version}</version>
                    </dependency>
                </dependencies>
            </plugin>

            <!-- Hot reloading with JRebel -->
            <plugin>
                <groupId>org.zeroturnaround</groupId>
                <artifactId>jrebel-maven-plugin</artifactId>
                <version>${jrebel.version}</version>
                <executions>
                    <execution>
                        <id>generate-rebel-xml</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>generate</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <!-- For more information about how to configure JRebel plugin see:
                         http://manuals.zeroturnaround.com/jrebel/standalone/maven.html#maven-rebel-xml -->
                    <classpath>
                        <fallback>all</fallback>
                        <resources>
                            <resource>
                                <!--
                                Empty resource element marks default configuration. By
                                default it is placed first in generated configuration.
                                -->
                            </resource>
                        </resources>
                    </classpath>

                    <!--
                      alwaysGenerate - default is false
                      If 'false' - rebel.xml is generated if timestamps of pom.xml and the current rebel.xml file are not equal.
                      If 'true' - rebel.xml will always be generated
                    -->
                    <alwaysGenerate>true</alwaysGenerate>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.0.1</version>
                <configuration>
                    <encoding>UTF-8</encoding>
                    <nonFilteredFileExtensions> <!-- jpg, jpeg, gif, bmp and png are added automatically -->
                        <nonFilteredFileExtension>ftl</nonFilteredFileExtension>
                        <nonFilteredFileExtension>acp</nonFilteredFileExtension>
                        <nonFilteredFileExtension>svg</nonFilteredFileExtension>
                        <nonFilteredFileExtension>pdf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>doc</nonFilteredFileExtension>
                        <nonFilteredFileExtension>docx</nonFilteredFileExtension>
                        <nonFilteredFileExtension>xls</nonFilteredFileExtension>
                        <nonFilteredFileExtension>xlsx</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ppt</nonFilteredFileExtension>
                        <nonFilteredFileExtension>pptx</nonFilteredFileExtension>
                        <nonFilteredFileExtension>bin</nonFilteredFileExtension>
                        <nonFilteredFileExtension>lic</nonFilteredFileExtension>
                        <nonFilteredFileExtension>swf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>zip</nonFilteredFileExtension>
                        <nonFilteredFileExtension>msg</nonFilteredFileExtension>
                        <nonFilteredFileExtension>jar</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ttf</nonFilteredFileExtension>
                        <nonFilteredFileExtension>eot</nonFilteredFileExtension>
                        <nonFilteredFileExtension>woff</nonFilteredFileExtension>
                        <nonFilteredFileExtension>woff2</nonFilteredFileExtension>
                        <nonFilteredFileExtension>css</nonFilteredFileExtension>
                        <nonFilteredFileExtension>ico</nonFilteredFileExtension>
                        <nonFilteredFileExtension>psd</nonFilteredFileExtension>
                        <nonFilteredFileExtension>js</nonFilteredFileExtension>
                    </nonFilteredFileExtensions>
                </configuration>
            </plugin>
        </plugins>

        <resources>
            <!-- Filter the resource files in this project and do property substitutions -->
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <testResources>
            <!-- Filter the test resource files in this project and do property substitutions -->
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>
        <pluginManagement>
        	<plugins>
        		<!--This plugin's configuration is used to store Eclipse m2e settings only. It has no influence on the Maven build itself.-->
        		<plugin>
        			<groupId>org.eclipse.m2e</groupId>
        			<artifactId>lifecycle-mapping</artifactId>
        			<version>1.0.0</version>
        			<configuration>
        				<lifecycleMappingMetadata>
        					<pluginExecutions>
        						<pluginExecution>
        							<pluginExecutionFilter>
        								<groupId>
        									org.zeroturnaround
        								</groupId>
        								<artifactId>
        									jrebel-maven-plugin
        								</artifactId>
        								<versionRange>
        									[1.1.6,)
        								</versionRange>
        								<goals>
        									<goal>generate</goal>
        								</goals>
        							</pluginExecutionFilter>
        							<action>
        								<ignore />
        							</action>
        						</pluginExecution>
        					</pluginExecutions>
        				</lifecycleMappingMetadata>
        			</configuration>
        		</plugin>
        	</plugins>
        </pluginManagement>
    </build>

    <!--
        Alfresco Maven Repositories
        -->
    <repositories>
        <repository>
            <id>alfresco-public</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/public</url>
        </repository>
        <repository>
            <id>alfresco-public-snapshots</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/public-snapshots</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
        </repository>
        <!-- Alfresco Enterprise Edition Artifacts, put username/pwd for server in settings.xml -->
        <repository>
            <id>alfresco-private-repository</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/private</url>
        </repository>
    </repositories>
    <pluginRepositories>
        <pluginRepository>
            <id>alfresco-plugin-public</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/public</url>
        </pluginRepository>
        <pluginRepository>
            <id>alfresco-plugin-public-snapshots</id>
            <url>https://artifacts.alfresco.com/nexus/content/groups/public-snapshots</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>daily</updatePolicy>
            </snapshots>
        </pluginRepository>
    </pluginRepositories>

</project>