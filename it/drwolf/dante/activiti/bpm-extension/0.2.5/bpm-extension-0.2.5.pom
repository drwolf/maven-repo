<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>it.drwolf.dante.activiti</groupId>
	<artifactId>bpm-extension</artifactId>
	<version>0.2.5</version>
	
	<properties>
		<activiti-engine.version>6.0.0</activiti-engine.version>
		<jgitflow.version>1.0-m5.1</jgitflow.version>
		<slf4j-log4j12.version>1.7.6</slf4j-log4j12.version>
		<slf4j-api.version>1.7.6</slf4j-api.version>
		<async-http-client.version>2.0.35</async-http-client.version>
		<jackson-core.version>2.7.5</jackson-core.version>
		<artifacts.folder>${project.build.directory}/libs-to-deploy</artifacts.folder>
	</properties>
	
	<distributionManagement>
		<repository>
			<id>mavenrepo</id>
			<name>MavenRepo</name>
			<url>git:master://git@bitbucket.org:drwolf/maven-repo.git</url>
		</repository>
	</distributionManagement>

	<pluginRepositories>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>
	
	<!-- 
	Prima di aggiungere una dipendenza controllare in ACTIVITI_TOMCAT_HOME/webapps/activiti-rest/WEB-INF/lib
	Se la dipendenza è già presente mettere la stessa versione e settare lo scope a "provided"
	-->
	<dependencies>
		<dependency>
			<groupId>org.activiti</groupId>
			<artifactId>activiti-engine</artifactId>
			<version>${activiti-engine.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>${slf4j-api.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>${slf4j-log4j12.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.asynchttpclient</groupId>
			<artifactId>async-http-client</artifactId>
			<version>${async-http-client.version}</version>
		</dependency>
		<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-core -->
		<dependency>
    			<groupId>com.fasterxml.jackson.core</groupId>
    			<artifactId>jackson-core</artifactId>
    			<version>${jackson-core.version}</version>
    			<scope>provided</scope>
		</dependency>
	</dependencies>
	
	<repositories>
		<repository>
			<id>drwolf-maven-repo</id>
			<name>DrWolf maven-repo</name>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<url>https://api.bitbucket.org/1.0/repositories/drwolf/maven-repo.git/raw/master</url>
		</repository>
	</repositories>

	<build>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>0.3.0</version><!-- you might need to check for a new version -->
			</extension>
		</extensions>
		<resources>
		    <resource>
		        <directory>src/main/resources/bpm</directory>
		        <filtering>true</filtering>
		        <excludes>
		            <exclude>**/*</exclude>
		        </excludes>
		    </resource>
		    <resource>
		        <directory>src/main/resources</directory>
		        <filtering>true</filtering>
		        <includes>
		            <include>*.properties</include>
		        </includes>
		    </resource>
		</resources>
		<plugins>

			<!-- download source code in Eclipse, best practice -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-eclipse-plugin</artifactId>
				<version>2.9</version>
				<configuration>
					<downloadSources>true</downloadSources>
					<downloadJavadocs>false</downloadJavadocs>
				</configuration>
			</plugin>

			<!-- Set a compiler level -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			
			<plugin>
				<groupId>external.atlassian.jgitflow</groupId>
				<artifactId>jgitflow-maven-plugin</artifactId>
				<version>${jgitflow.version}</version>
				<configuration>
					<flowInitContext>
						<versionTagPrefix>v</versionTagPrefix>
					</flowInitContext>
				</configuration>
			</plugin>
			
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-dependency-plugin</artifactId>
				<executions>
					<execution>
						<id>copy-dependencies</id>
						<phase>package</phase>
						<goals>
							<goal>copy-dependencies</goal>
						</goals>
						<configuration>
							<excludeScope>provided</excludeScope>
							<outputDirectory>${artifacts.folder}</outputDirectory>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.0.2</version>
			</plugin>

		</plugins>
	</build>
</project>