<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>
	<groupId>it.drwolf</groupId>
	<artifactId>dante-common-lib</artifactId>
	<version>0.5.0</version>

	<packaging>jar</packaging>

	<properties>
		<junit.version>4.12</junit.version>
		<java-jwt.version>3.1.0</java-jwt.version>
		<logback.version>1.2.2</logback.version>
		<jgitflow.version>1.0-m5.1</jgitflow.version>
		<mockito.version>1.10.19</mockito.version>
		<play-java-ws_2.11.version>2.5.12</play-java-ws_2.11.version>
		<play-java-jpa_2.11.version>2.5.12</play-java-jpa_2.11.version>
		<jackson-module-jsonSchema.version>2.8.6</jackson-module-jsonSchema.version>
		<!-- elide-core.version>3.0.4</elide-core.version>
		<elide-datastore-hibernate5.version>3.0.4</elide-datastore-hibernate5.version-->
		<hibernate-core.version>5.2.15.Final</hibernate-core.version>
		<hibernate-entitymanager.version>5.2.15.Final</hibernate-entitymanager.version>
		<apache.poi.version>3.16</apache.poi.version>
	</properties>

	<distributionManagement>
		<repository>
			<id>mavenrepo</id>
			<name>MavenRepo</name>
			<url>git:master://git@bitbucket.org:drwolf/maven-repo.git</url>
		</repository>
	</distributionManagement>

	<pluginRepositories>
		<pluginRepository>
			<id>synergian-repo</id>
			<url>https://raw.github.com/synergian/wagon-git/releases</url>
		</pluginRepository>
	</pluginRepositories>

	<dependencies>
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>${junit.version}</version>
		</dependency>
		<dependency>
			<groupId>com.auth0</groupId>
			<artifactId>java-jwt</artifactId>
			<version>${java-jwt.version}</version>
		</dependency>
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
			<version>${logback.version}</version>
		</dependency>
		<dependency>
			<groupId>com.fasterxml.jackson.module</groupId>
			<artifactId>jackson-module-jsonSchema</artifactId>
			<version>${jackson-module-jsonSchema.version}</version>
		</dependency>
		<dependency>
			<groupId>com.typesafe.play</groupId>
			<artifactId>play-java-ws_2.11</artifactId>
			<version>${play-java-ws_2.11.version}</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>com.typesafe.play</groupId>
			<artifactId>play-java-jpa_2.11</artifactId>
			<version>${play-java-jpa_2.11.version}</version>
		</dependency>
		<dependency>
			<groupId>org.hibernate.javax.persistence</groupId>
			<artifactId>hibernate-jpa-2.1-api</artifactId>
			<version>1.0.0.Final</version>
			<scope>provided</scope>
		</dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-all</artifactId>
			<version>${mockito.version}</version>
			<scope>test</scope>
		</dependency>
		<!-- https://mvnrepository.com/artifact/com.yahoo.elide/elide-core -->
		<!-- <dependency>
			<groupId>com.yahoo.elide</groupId>
			<artifactId>elide-core</artifactId>
			<version>${elide-core.version}</version>
		</dependency> -->
		<!-- https://mvnrepository.com/artifact/com.yahoo.elide/elide-datastore-hibernate5 -->
		<!-- <dependency>
			<groupId>com.yahoo.elide</groupId>
			<artifactId>elide-datastore-hibernate5</artifactId>
			<version>${elide-datastore-hibernate5.version}</version>
		</dependency> -->
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-core</artifactId>
			<version>${hibernate-core.version}</version>
		</dependency>
		<dependency>
			<groupId>org.hibernate</groupId>
			<artifactId>hibernate-entitymanager</artifactId>
			<version>${hibernate-entitymanager.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi</artifactId>
			<version>${apache.poi.version}</version>
		</dependency>
		<dependency>
			<groupId>org.apache.poi</groupId>
			<artifactId>poi-ooxml</artifactId>
			<version>${apache.poi.version}</version>
		</dependency>
	</dependencies>

	<repositories>
		<repository>
			<id>drwolf-maven-repo</id>
			<name>DrWolf maven-repo</name>
			<releases>
				<enabled>true</enabled>
			</releases>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<url>https://api.bitbucket.org/1.0/repositories/drwolf/maven-repo.git/raw/master</url>
		</repository>
	</repositories>

	<build>
		<extensions>
			<extension>
				<groupId>ar.com.synergian</groupId>
				<artifactId>wagon-git</artifactId>
				<version>0.3.0</version><!-- you might need to check for a new version -->
			</extension>
		</extensions>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-jar-plugin</artifactId>
				<version>3.0.2</version>
			</plugin>
			<plugin>
				<groupId>external.atlassian.jgitflow</groupId>
				<artifactId>jgitflow-maven-plugin</artifactId>
				<version>${jgitflow.version}</version>
				<configuration>
					<flowInitContext>
						<versionTagPrefix>v</versionTagPrefix>
					</flowInitContext>
				</configuration>
			</plugin>
		</plugins>
	</build>

</project>
